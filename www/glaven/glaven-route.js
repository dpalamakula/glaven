(function() {
    'use strict';

    angular
        .module('glaven')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfig($stateProvider,$urlRouterProvider) {
      $stateProvider
        .state('login', {
        url: "/login",
        templateUrl: "glaven/authentication/login.html"
      });

      $urlRouterProvider.otherwise('/login');
    };
})();
