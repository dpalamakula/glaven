(function(){

  'use strict';

  angular
    .module('glaven.dashboard')
      .directive('appOpenAnimate',AppOpenAnimate);

  AppOpenAnimate.$inject = ['$ionicGesture', '$state'];

  //directive
  function AppOpenAnimate ($ionicGesture, $state){
    var directive = {
        link: link,
        restrict: 'A',
        scope:{
          navigationPath:'@appOpenAnimate'
        }
     };

     return directive;

     function link(scope, element, attrs) {
       $ionicGesture.on('tap',AnimateForTap, element);

       var scaleTo = 10, animateTime = '0.5s';

       function AnimateForTap(){
         move(element[0])
         .scale(scaleTo)
         .end(function(){
            $state.go(scope.navigationPath);

             //scale back
             move(element[0])
             .scale(1)
             .end();
         });
       };
     }; //end link function
  }; //end directive
})();
