(function() {
    'use strict';

    angular
        .module('glaven.people')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {

      $stateProvider
        .state('glaven.people', {
          abstract: true,
          url: "/people",
          views:{
            "mainContent":{
              templateUrl: "glaven/people/views/peopleTabs.html",
            }
          }
        })
        .state('glaven.people.list', {
          url: "/list",
          views:{
            "tab-peopleList":{
              templateUrl: "glaven/people/views/peopleList.html"
            }
          }
        })
        .state('glaven.people.map', {
          url: "/map",
          views:{
            "tab-peopleMap":{
              templateUrl: "glaven/people/views/peopleMap.html"
            }
          }
        })
        .state('glaven.people.detail', {
          url: "/detail/:username",
          views:{
            "tab-peopleCard":{
              templateUrl: "glaven/people/views/personDetail.html"
            }
          }

        })
        .state('glaven.people.card', {
          url: "/card",
          views:{
            "tab-peopleCard":{
              templateUrl: "glaven/people/views/peopleCard.html"
            }
          }
        });
    };
})();
