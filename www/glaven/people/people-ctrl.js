(function(){

  'use strict';

  angular
    .module('glaven.people')
      .controller('PeopleCtrl',PeopleCtrl);

  PeopleCtrl.$inject = ['PeopleService','MapService', '$state', '$scope'];

  function PeopleCtrl(PeopleService,MapService, $state, $scope){
    var vm = this;

    vm.loadPeople = function(){
      PeopleService.getPeople().then(function(response){
        vm.people = response;
        loadMapMarkers();
      }).finally(function(){
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    vm.onPersonClicked = function(username){
      vm.selectedPerson = _(vm.people).find({'username':username});
      $state.go('glaven.people.detail', {username: username} );
    };

    //map defaults
    vm.map = {center: { latitude: 39.8282, longitude: -98.5795 }, zoom: 4 };
    function loadMapMarkers(){
      vm.peopleMapMarkers = [];
      MapService.getMapMarkersForPeople(vm.people).map(function(promise){
        promise.then(function(data){
          vm.peopleMapMarkers.push(data);
        });
      });

    };

    vm.loadPeople();

  };

})();
