(function() {
  'use strict';

  angular
    .module("glaven.people")
    .factory('MapService', MapService);

  MapService.$inject = ['$http', '$ionicLoading', '$q', '$log'];

  function MapService($http, $ionicLoading, $q, $log) {

    function getMapMarkersForPeople(people) {
      var promises = [];

      angular.forEach(people, function(person) {

        var promise = $http.get('http://api.zippopotam.us/us/' + person.location.zip).then(function(response) {
          return createMarkerForPerson(person, response.data);
        });
        promises.push(promise);
      });
      return promises;
    };


    function createMarkerForPerson(person, zipData) {
      if (zipData !== undefined && zipData.places !== undefined && zipData.places.length > 0) {
        var place = zipData.places[0];

        var personMarker = {
          map: {
            coordinates: {
              latitude: place.latitude,
              longitude: place.longitude,
              showWindow: true
            },
            options: {
              labelAnchor: "100 0",
              labelClass: "marker-labels"
            }
          },
          profile: {
            thumbnail: person.picture.thumbnail,
            fullname: person.name.first + ' ' + person.name.last
          }
        };
        return personMarker;
      }
    };


    return {
      getMapMarkersForPeople: getMapMarkersForPeople,
    };
  };
})();
