(function(){
  'use strict';

  angular
    .module("glaven.people")
      .factory('PeopleService',PeopleService);

      PeopleService.$inject = ['$http', '$ionicLoading', '$q', '$log'];

  function PeopleService($http, $ionicLoading, $q){

    function getPeople(){
      $ionicLoading.show({template:'Loading...'})

      //using $http
      var deferred = $q.defer();
      $http.get('http://api.randomuser.me/?results=10&nat=us')
              .success(function(data){
                var people = _(data.results)
                              .pluck('user')
                              .value();

                $ionicLoading.hide();

                deferred.resolve(people);
              })
              .error(function(err){
                $log.error(err);
                $ionicLoading.hide();
                deferred.reject();
              });
      return deferred.promise;
    };

    function addMapMarkerToPerson(person){
      var deferred = $q.defer();
      $http.get('http://api.zippopotam.us/us/' + person.location.zip)
        .success(function(data){
          if(data!== undefined && data.places !== undefined && data.places.length > 0){
            var place = data.places[0];

            person.mapMarker = {
              latitude: place.latitude,
              longitude: place.longitude,
              showWindow:true,
              options : {
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
              }

            };
          }
        })
        .error(function(){
          //log error
        });
    };

    return {
      getPeople:getPeople,
    };
  };
})();
