(function(){

  'use strict';

  angular
    .module('glaven.people')
      .directive('swipeOpenEmail',SwipeOpenEmail);

  SwipeOpenEmail.$inject = ['$ionicGesture'];

  //directive
  function SwipeOpenEmail ($ionicGesture){
    var directive = {
        link: link,
        restrict: 'A',
        scope:{
          emailAddress: '@swipeOpenEmail'
        }
     };

     return directive;

     function link(scope, element, attrs) {
       $ionicGesture.on('swipeleft',AnimateLeftAndOpenEmail, element);
       var slideTo = -120, animateTime = '0.5s';

       function AnimateLeftAndOpenEmail(){
         var moveBack = move(element[0])
            .duration(animateTime)
            .x(0);

        //animation to slide left
         move(element[0])
          .x(slideTo)
          .then(moveBack)
          .duration(animateTime)
          .end(function(){
            //open external app for email
            window.open('mailto:' + scope.emailAddress, '_system');
          });

       };

     };
  };
})();
