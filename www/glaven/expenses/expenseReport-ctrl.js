(function() {

  'use strict';

  angular
    .module('glaven.expenses')
    .controller('ExpenseReportCtrl', ExpenseReportCtrl);

  ExpenseReportCtrl.$inject = ['$state', '$stateParams', 'ExpenseService', 'DatePicker', 'CameraSvc'];

  //demo code
  function ExpenseReportCtrl($state, $stateParams, ExpenseService, DatePicker, CameraSvc) {
    var vm = this;
    vm.id = $stateParams.id;
    vm.newExpense = ExpenseService.newExpense();

    vm.selectedReport = ExpenseService.getReport(vm.id);

    vm.addExpenseItem = function() {
      ExpenseService.addExpenseItem(vm.id, vm.newExpense);
      vm.newExpense = ExpenseService.newExpense();
      $state.go('glaven.expenses.items', {
        id: vm.id
      });
    };

    vm.datePicker = function() {
      DatePicker.getDate().then(function(date) {
        vm.newExpense.date = date;
      }, function(err) {
        alert(err);
      });
    };

    vm.takePicture = function() {
      CameraSvc.getPicture().then(function(imageData) {
        vm.newExpense.imgSrc = "data:image/jpeg;base64," + imageData;
      }, function(err) {
        alert('error: ' + err);
      });
    };
  };

})();
