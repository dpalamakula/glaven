(function(){

  'use strict';

  angular
    .module('glaven.expenses',[
    ]).config(function($compileProvider){
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
      });
})();
