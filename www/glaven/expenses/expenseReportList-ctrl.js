(function() {

  'use strict';

  angular
    .module('glaven.expenses')
    .controller('ExpenseReportListCtrl', ExpenseReportListCtrl);

  ExpenseReportListCtrl.$inject = ['$state','$ionicPopup', 'ExpenseService'];

  //demo code
  function ExpenseReportListCtrl($state,$ionicPopup, ExpenseService) {
    var vm = this;

    vm.newReport = ExpenseService.newReport();

    vm.expenseReports = ExpenseService.getExpenseReports();

    vm.addExpenseReport = function() {
      if (!vm.newReport.title) {
        $ionicPopup.alert({
          title: 'Report name is required'
        });
        return;
      }
      ExpenseService.addExpenseReport(vm.newReport);
      vm.newReport = ExpenseService.newReport();
    };
  };

})();
