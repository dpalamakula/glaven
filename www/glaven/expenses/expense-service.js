(function() {

  'use strict';

  angular
    .module('glaven.expenses')
    .factory('ExpenseService', ExpenseService);

  ExpenseService.$inject = [];

  function ExpenseService() {
    var self = this;
    var expenseReports = [];
    var currentId = 1;
    //dummy data
    expenseReports.push(new ExpenseReport(currentId++, 'JAX Trip'));
    expenseReports.push(new ExpenseReport(currentId++, 'Neogov Conference'));

    function getExpenseReports() {
      return expenseReports;
    };

    function newReport() {
      return new ExpenseReport(currentId + 1, '');
    };

    function newExpense() {
      return new Expense('', null, new Date(), null);
    };

    function getReport(id) {
      return _(expenseReports).filter({
        id: Number(id)
      }).first();
    };

    function addExpenseReport(report) {
      expenseReports.push(report);
    };

    function addExpenseItem(id, expense) {
      var report = _(expenseReports).filter({
        id: Number(id)
      }).first();
      if (report)
        report.addExpense(expense);
    };

    return {
      newReport: newReport,
      newExpense: newExpense,
      getExpenseReports: getExpenseReports,
      getReport: getReport,
      addExpenseReport: addExpenseReport,
      addExpenseItem: addExpenseItem
    };
  };


  //model classes
  function Expense(title, amount, date, categoryId) {
    this.title = title;
    this.amount = amount;
    this.date = date;
    this.categoryId = categoryId;
    this.imgSrc = 'http://placehold.it/50x50';
  };

  function ExpenseReport(id, title) {
    this.id = id;
    this.title = title;
    this.totalAmount = function() {
      return _.sum(expenses, function(expense) {
          return expense.amount;
        });
      };
    var expenses = [];

    this.getExpenses = function() {
      return expenses;
    };

    this.hasExpenses = function(){
      return expenses.length > 0;
    };

    this.addExpense = function(expense) {
      expenses.push(expense);
    };
  };
})();
