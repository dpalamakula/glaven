(function() {
    'use strict';

    angular
        .module('glaven.expenses')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {

      $stateProvider
        .state('glaven.expenses', {
          abstract: true,
          url: "/expense",
          views:{
            "mainContent":{
              templateUrl: "glaven/expenses/views/expenseTabs.html",
            }
          }
        })
        .state('glaven.expenses.reports', {
          url: "/reports",
          views:{
            "tab-expenseReports":{
              templateUrl: "glaven/expenses/views/expenseReports.html"
            }
          }
        })
        .state('glaven.expenses.items', {
          url: "/:id/items",
          views:{
            "tab-expenseReports":{
              templateUrl: "glaven/expenses/views/expenseItems.html"
            }
          }
        })
        .state('glaven.expenses.add', {
          url: "/:id/items/add",
          views:{
            "tab-expenseReports":{
              templateUrl: "glaven/expenses/views/addExpense.html"
            }
          }
        });
    };
})();
