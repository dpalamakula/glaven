(function(){

  'use strict';

  angular
    .module('glaven',[
        'ionic',
        'ngCordova',
        'glaven.common',
        'glaven.dashboard',
        'glaven.people',
        'glaven.expenses',
        'restangular'

    ]).run(function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
      });
    }).config(function($ionicConfigProvider,$compileProvider) {
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile|content):|data:image\//);
      //show tabs at the bottom
      $ionicConfigProvider.tabs.position('bottom');

    });
})();
