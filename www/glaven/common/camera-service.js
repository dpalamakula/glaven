(function() {

  'use strict';

  angular
    .module('glaven')
    .factory('CameraSvc', CameraSvc);

    CameraSvc.$inject = ['$q', '$cordovaCamera'];

  function CameraSvc($q, $cordovaCamera) {
    return {
      getPicture: function(options) {
        var deferred = $q.defer();
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          deferred.resolve(imageData);
        }, function(err) {
          deferred.reject(err);
        });
        return deferred.promise;
      }
    };
  };

})();
