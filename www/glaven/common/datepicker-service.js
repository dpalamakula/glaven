(function() {

  'use strict';

  angular
    .module('glaven.common')
    .factory('DatePicker', DatePicker);

  DatePicker.$inject = ['$q', '$cordovaDatePicker'];

  function DatePicker($q, $cordovaDatePicker) {
    return {
      getDate: function(options) {
        var q = $q.defer();
        var options = {
          date: new Date(),
          mode: 'date', // or 'time'
          minDate: new Date() - 10000,
          allowOldDates: true,
          allowFutureDates: false,
          doneButtonLabel: 'DONE',
          doneButtonColor: '#F2F3F4',
          cancelButtonLabel: 'CANCEL',
          cancelButtonColor: '#000000'
        };

        $cordovaDatePicker.show(options).then(function(date){
          q.resolve(date);
        },function(err){
          q.reject(err);
        });

        return q.promise;
      }
    }
  };

})();
