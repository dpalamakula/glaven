(function() {
    'use strict';

    angular
        .module('glaven.common')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {

      $stateProvider
        .state('glaven', {
          abstract: true,
          url: "",
          templateUrl: "glaven/common/layout/layout.html"
        })
        .state('glaven.dashboard', {
          url: "/dashboard",
          views:{
            "mainContent":{
              templateUrl: "glaven/dashboard/dashboard.html",
            }
          }
        });
    };
})();
