(function() {

  'use strict';

  angular
    .module('glaven.common')
    .filter('titleCase', titleCase);

  //filter to replace all chars except numbers
  function numbersOnly() {
    return input.val().replace(/\D+/, '');
  };

  //filter to convert the first character of a word to uppercase
  function titleCase() {
    return function(input) {
      return (!!input) ? input.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  };
})();
