(function(){

  'use strict';

  angular
    .module('glaven')
      .controller('LoginCtrl',LoginCtrl);

  LoginCtrl.$inject = ['$state'];

  function LoginCtrl($state){
    var vm = this;

    vm.username = '';
    vm.password='';
    vm.login = function(){
      //TODO: call service to get token and save it in window.sessionStorage or localStorage
      //based on need. reference: https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/
      if(vm.username === 'demo' && vm.password === 'demo'){
          $state.go('glaven.dashboard')
      }
      else{
        alert('Login Failed');
      }

    };
  };

})();
